part of 'shared.dart';

Color mainColor = Color(0xFFFF5E2C);
Color blueColor = Color(0xFF5BD8FF);
Color purpleColor = Color(0xFF7028AE);
Color redColor = Color(0xFFFF5C83);

TextStyle whiteTextStyle1 = GoogleFonts.poppins()
    .copyWith(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 20);

TextStyle whiteTextStyle2 = GoogleFonts.poppins()
    .copyWith(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 20);

TextStyle whiteTextStyle3 = GoogleFonts.poppins()
    .copyWith(color: Colors.white, fontWeight: FontWeight.w300);

TextStyle walletTextStyle = GoogleFonts.openSans().copyWith(
  fontSize: 26,
  fontWeight: FontWeight.w600,
  color: Colors.white,
);

const double defaultMargin = 24;

String pictureTestUrl =
    'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80';
