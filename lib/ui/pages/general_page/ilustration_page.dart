part of '../pages.dart';

class IlustrationPage extends StatelessWidget {
  final String title;
  final String subtitle;
  final String picturePath;

  IlustrationPage({
    @required this.title,
    @required this.subtitle,
    @required this.picturePath,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 150,
            height: 150,
            margin: EdgeInsets.only(bottom: 50),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.7),
                  spreadRadius: 1,
                  blurRadius: 100,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              image: DecorationImage(
                image: AssetImage(picturePath),
              ),
            ),
          ),
          Text(
            title,
            style: whiteTextStyle2.copyWith(fontSize: 20),
          ),
          Text(
            subtitle,
            style: whiteTextStyle3.copyWith(fontSize: 16),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
