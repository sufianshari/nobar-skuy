part of 'pages.dart';

class SeatPage extends StatefulWidget {
  @override
  _SeatPageState createState() => _SeatPageState();
}

class _SeatPageState extends State<SeatPage> {
  List<String> selectedSeats = [];

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      subtitle: 'Kimetsu No Yaiba',
      textStyle: whiteTextStyle2,
      onBackButtonPressed: () {
        Get.back();
      },
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 50, bottom: 51),
            width: double.infinity,
            height: 84,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/screen.png'),
              ),
            ),
          ),
          generateSeats(),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 6),
                    width: 17,
                    height: 17,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.transparent,
                      border: Border.all(color: Colors.white),
                    ),
                  ),
                  Text('Available',
                      style: whiteTextStyle2.copyWith(fontSize: 12)),
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 6),
                    width: 17,
                    height: 17,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: mainColor,
                    ),
                  ),
                  Text('Selected',
                      style: whiteTextStyle2.copyWith(fontSize: 12)),
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 6),
                    width: 17,
                    height: 17,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: purpleColor,
                      border: Border.all(color: Colors.white),
                    ),
                  ),
                  Text('Unavailable',
                      style: whiteTextStyle2.copyWith(fontSize: 12)),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          CircleButton(
            color: selectedSeats.length > 0 ? mainColor : Color(0xFFE4E4E4),
            onPressed: () {
              Get.to(CheckoutPage());
            },
          ),
        ],
      ),
    );
  }

  Column generateSeats() {
    List<int> numberofSeats = [3, 5, 5, 5, 5];
    List<Widget> widgets = [];

    for (int i = 0; i < numberofSeats.length; i++) {
      widgets.add(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(
            numberofSeats[i],
            (index) => Padding(
                  padding: EdgeInsets.only(
                      right: index < numberofSeats[i] - 1 ? 16 : 0, bottom: 16),
                  child: SelectableBox(
                    "${String.fromCharCode(i + 65)}${index + 1}",
                    width: 40,
                    height: 40,
                    textStyle: whiteTextStyle3,
                    isSelected: selectedSeats
                        .contains("${String.fromCharCode(i + 65)}${index + 1}"),
                    onTap: () {
                      String seatNumber =
                          "${String.fromCharCode(i + 65)}${index + 1}";
                      setState(() {
                        if (selectedSeats.contains(seatNumber)) {
                          selectedSeats.remove(seatNumber);
                        } else {
                          selectedSeats.add(seatNumber);
                        }
                      });
                    },
                    isEnabled: index != 0,
                  ),
                )),
      ));
    }

    return Column(
      children: widgets,
    );
  }
}
