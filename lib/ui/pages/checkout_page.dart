part of 'pages.dart';

class CheckoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      onBackButtonPressed: () {},
      subtitle: 'Checkout Page',
      textStyle: whiteTextStyle2,
      child: Column(
        children: [
          SizedBox(
            height: 40,
          ),
          Row(
            children: [
              Container(
                width: 70,
                height: 90,
                margin: EdgeInsets.only(left: defaultMargin, right: 20),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.white.withOpacity(0.7),
                        spreadRadius: 1,
                        blurRadius: 70,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.circular(8),
                    image: DecorationImage(
                        image: NetworkImage(
                          'https://cdn0-production-images-kly.akamaized.net/5qJqGCk1744Y3FShvYIdC7n5keI=/640x853/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3485854/original/033690000_1623975380-Approved_-_A4Poster_ENG.JPG',
                        ),
                        fit: BoxFit.cover)),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: Text(
                        'Kimetsu No Yaiba',
                        style: whiteTextStyle2.copyWith(fontSize: 16),
                        maxLines: 2,
                        overflow: TextOverflow.clip,
                      )),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.3,
                    margin: EdgeInsets.symmetric(vertical: 6),
                    child: Text(
                      "Japanese",
                      style: whiteTextStyle3.copyWith(
                        fontSize: 12,
                      ),
                    ),
                  ),
                  RatingStars(3)
                ],
              )
            ],
          ),
          Container(
              margin:
                  EdgeInsets.symmetric(vertical: 20, horizontal: defaultMargin),
              child: Divider(
                color: Color(0xFFE4E4E4),
                thickness: 1,
              )),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Order ID", style: whiteTextStyle3.copyWith(fontSize: 16)),
                Text(
                  " 32442",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 9,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Cinema",
                    style: whiteTextStyle3.copyWith(
                        fontSize: 16, fontWeight: FontWeight.w400)),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.55,
                  child: Text(
                    "Cinepolis",
                    textAlign: TextAlign.end,
                    style: whiteTextStyle3.copyWith(
                        fontSize: 16, fontWeight: FontWeight.w400),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 9,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Date & Time",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                ),
                Text(
                  "20.00",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 9,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Seat Numbers",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.2,
                  child: Text(
                    "A2",
                    textAlign: TextAlign.end,
                    style: whiteTextStyle3.copyWith(
                      fontSize: 16,
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 9,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Price",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                ),
                Text(
                  "IDR 25.000",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 9,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Fee",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                ),
                Text(
                  "IDR 1.500 ",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 9,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Total",
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                ),
                Text(
                  'IDR 32.000',
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
          Container(
              margin:
                  EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 20),
              child: Divider(
                color: Color(0xFFE4E4E4),
                thickness: 1,
              )),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Your Wallet",
                    style: whiteTextStyle3.copyWith(
                      fontSize: 16,
                    )),
                Text(
                  'IDR 40.000',
                  style: whiteTextStyle3.copyWith(
                    fontSize: 16,
                    color: blueColor,
                  ),
                )
              ],
            ),
          ),
          CustomButton(
            margin: EdgeInsets.only(top: 60),
            title: 'Checkout Now',
            onPressed: () {
              Get.to(SuccesCheckoutPage());
            },
            width: 226,
          ),
          CustomButton(
            margin: EdgeInsets.only(top: 60, bottom: 100),
            title: 'Top Up My Wallet',
            onPressed: () {
              Get.to(SuccessTopUpPage());
            },
            width: 226,
          ),
        ],
      ),
    );
  }
}
