part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: 'Sign In',
      textStyle: whiteTextStyle1.copyWith(fontSize: 34),
      child: Column(
        children: [
          CustomInputText(
            title: 'E-mail',
            hintText: 'Enter your e-mail',
          ),
          CustomInputText(
            obsecureText: true,
            title: 'Password',
            hintText: 'Enter your password',
          ),
          CustomButton(
            margin: EdgeInsets.symmetric(vertical: 47),
            width: 248,
            title: 'Sign In',
            onPressed: () {
              Get.to(MainPage());
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Don't hava acount yet?",
                  style: whiteTextStyle2.copyWith(
                      fontSize: 14, color: Colors.white.withOpacity(0.9))),
              TextButton(
                onPressed: () {
                  Get.to(SignUpPage());
                },
                child: Text('Register',
                    style: whiteTextStyle2.copyWith(
                        fontSize: 14, color: Colors.white.withOpacity(0.9))),
              )
            ],
          )
        ],
      ),
    );
  }
}
