part of 'pages.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Widget generateDashedDivider(double width) {
    int n = width ~/ 5;
    return Row(
      children: List.generate(
          n,
          (index) => (index % 2 == 0)
              ? Container(
                  height: 2,
                  width: width / n,
                  color: Color(0xFFE4E4E4),
                )
              : SizedBox(
                  width: width / n,
                )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      onBackButtonPressed: () {},
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 74, bottom: 10),
            width: 120,
            height: 120,
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white.withOpacity(0.7),
                          spreadRadius: 1,
                          blurRadius: 70,
                          offset:
                              const Offset(0, 1), // changes position of shadow
                        ),
                      ],
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage("assets/default.png"),
                          fit: BoxFit.cover)),
                ),
              ],
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width - 2 * defaultMargin,
            child: Text(
              'Tirta Rachman',
              maxLines: 2,
              textAlign: TextAlign.center,
              overflow: TextOverflow.clip,
              style: whiteTextStyle1.copyWith(fontSize: 18),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 2 * defaultMargin,
            margin: EdgeInsets.only(top: 8, bottom: 30),
            child: Text(
              'tirta_ra@icloud',
              textAlign: TextAlign.center,
              style: whiteTextStyle3.copyWith(fontSize: 16),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  SizedBox(
                      width: 24,
                      height: 24,
                      child: Image.asset("assets/user.png")),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Edit Profile",
                    style: whiteTextStyle2.copyWith(fontSize: 16),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 16),
                child: generateDashedDivider(
                    MediaQuery.of(context).size.width - 2 * defaultMargin),
              ),
              Row(
                children: [
                  SizedBox(
                      width: 24,
                      height: 24,
                      child: Image.asset("assets/wallet.png")),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "My Wallet",
                    style: whiteTextStyle2.copyWith(fontSize: 16),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 16),
                child: generateDashedDivider(
                    MediaQuery.of(context).size.width - 2 * defaultMargin),
              ),
              Row(
                children: [
                  SizedBox(
                      width: 24,
                      height: 24,
                      child: Image.asset("assets/language.png")),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Change Language",
                    style: whiteTextStyle2.copyWith(fontSize: 16),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 16),
                child: generateDashedDivider(
                    MediaQuery.of(context).size.width - 2 * defaultMargin),
              ),
              Row(
                children: [
                  SizedBox(
                      width: 24,
                      height: 24,
                      child: Image.asset("assets/mic.png")),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Help Center",
                    style: whiteTextStyle2.copyWith(fontSize: 16),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 16),
                child: generateDashedDivider(
                    MediaQuery.of(context).size.width - 2 * defaultMargin),
              ),
              Row(
                children: [
                  SizedBox(
                      width: 24,
                      height: 24,
                      child: Image.asset("assets/rate.png")),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Rate Nobar Skuy",
                    style: whiteTextStyle2.copyWith(fontSize: 16),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 16),
                child: generateDashedDivider(
                    MediaQuery.of(context).size.width - 2 * defaultMargin),
              ),
            ],
          )
        ],
      ),
    );
  }
}
