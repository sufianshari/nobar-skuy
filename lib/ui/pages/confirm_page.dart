part of 'pages.dart';

class ConfrimPage extends StatefulWidget {
  @override
  State<ConfrimPage> createState() => _ConfrimPageState();
}

class _ConfrimPageState extends State<ConfrimPage> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      onBackButtonPressed: () {},
      subtitle: 'Confrim\nNew Account',
      textStyle: whiteTextStyle1,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 100, bottom: 100),
            width: 150,
            height: 150,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.7),
                  spreadRadius: 1,
                  blurRadius: 70,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage('assets/default.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Text(
            'Welcome\nTirta Rachman',
            style: whiteTextStyle1.copyWith(fontSize: 16),
            textAlign: TextAlign.center,
          ),
          CustomButton(
            margin: EdgeInsets.only(top: 100),
            width: 250,
            title: 'Create My Account',
            onPressed: () {
              Get.to(SeatPage());
            },
          ),
        ],
      ),
    );
  }
}
