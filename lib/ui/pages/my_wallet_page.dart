part of 'pages.dart';

class MyWalletPage extends StatefulWidget {
  @override
  _MyWalletPageState createState() => _MyWalletPageState();
}

class _MyWalletPageState extends State<MyWalletPage> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      subtitle: 'My Wallet',
      textStyle: whiteTextStyle2,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 26),
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                blurRadius: 24,
                spreadRadius: 16,
                color: Colors.white.withOpacity(0.15),
              )
            ]),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(18),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 40.0,
                  sigmaY: 40.0,
                ),
                child: Container(
                  height: 174,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(18),
                      border: Border.all(
                        width: 1.5,
                        color: Colors.white.withOpacity(0.2),
                      )),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 68),
                        child: Text(
                          'IDR 500.000',
                          style: walletTextStyle,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 24),
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Card Holder',
                                    style:
                                        whiteTextStyle3.copyWith(fontSize: 10)),
                                Row(
                                  children: [
                                    Text('Tirta Rachman',
                                        style: whiteTextStyle2.copyWith(
                                            fontSize: 10)),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: Image.asset('assets/check.png'),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Card ID',
                                    style:
                                        whiteTextStyle3.copyWith(fontSize: 10)),
                                Row(
                                  children: [
                                    Text('902932312312',
                                        style: whiteTextStyle2.copyWith(
                                            fontSize: 10)),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: Image.asset('assets/check.png'),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Text(
            'Recent Transaction',
            style: whiteTextStyle2.copyWith(fontSize: 14),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                ComingSoonCard(),
                const SizedBox(width: 18),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Kimetsu No Yaiba',
                      style: whiteTextStyle2.copyWith(fontSize: 14),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      child: Text(
                        'IDR 20.000',
                        style: whiteTextStyle2.copyWith(
                            fontSize: 12, color: redColor),
                      ),
                    ),
                    Text(
                      'Cinepolis',
                      style: whiteTextStyle2.copyWith(fontSize: 12),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                ComingSoonCard(),
                const SizedBox(width: 18),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Kimetsu No Yaiba',
                      style: whiteTextStyle2.copyWith(fontSize: 14),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      child: Text(
                        'IDR 20.000',
                        style: whiteTextStyle2.copyWith(
                            fontSize: 12, color: redColor),
                      ),
                    ),
                    Text(
                      'Cinepolis',
                      style: whiteTextStyle2.copyWith(fontSize: 12),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                ComingSoonCard(),
                const SizedBox(width: 18),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Kimetsu No Yaiba',
                      style: whiteTextStyle2.copyWith(fontSize: 14),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      child: Text(
                        'IDR 20.000',
                        style: whiteTextStyle2.copyWith(
                            fontSize: 12, color: redColor),
                      ),
                    ),
                    Text(
                      'Cinepolis',
                      style: whiteTextStyle2.copyWith(fontSize: 12),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                ComingSoonCard(),
                const SizedBox(width: 18),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Kimetsu No Yaiba',
                      style: whiteTextStyle2.copyWith(fontSize: 14),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      child: Text(
                        'IDR 20.000',
                        style: whiteTextStyle2.copyWith(
                            fontSize: 12, color: redColor),
                      ),
                    ),
                    Text(
                      'Cinepolis',
                      style: whiteTextStyle2.copyWith(fontSize: 12),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
