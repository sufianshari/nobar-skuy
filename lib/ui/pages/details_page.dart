part of 'pages.dart';

class DetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(right: 16),
                width: double.infinity,
                height: 250,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.7),
                      spreadRadius: 1,
                      blurRadius: 70,
                      offset: const Offset(0, 1), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                    image: NetworkImage(
                      'https://cdn0-production-images-kly.akamaized.net/5qJqGCk1744Y3FShvYIdC7n5keI=/640x853/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3485854/original/033690000_1623975380-Approved_-_A4Poster_ENG.JPG',
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  'Kimetsu No Yaiba',
                  style: whiteTextStyle2.copyWith(fontSize: 24),
                ),
              ),
              Text(
                'Action – English',
                style: whiteTextStyle2.copyWith(fontSize: 12),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      ArtisCard(),
                      ArtisCard(),
                      ArtisCard(),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 30, bottom: 4),
            child: Text(
              'Storyline',
              style: whiteTextStyle2.copyWith(fontSize: 14),
            ),
          ),
          Text(
            'Berlatar di Jepang pada zaman Taisho, Tanjiro Kamado adalah seorang bocah lelaki baik hati dan cerdas yang tinggal bersama keluarganya dan mencari uang dengan cara menjual arang. Semuanya berubah ketika keluarganya diserang dan dibantai oleh iblis. Tanjiro dan saudarinya Nezuko adalah satu-satunya yang selamat dari insiden tersebut, meskipun Nezuko sekarang adalah iblis—tetapi secara mengejutkan dia masih menunjukkan tanda-tanda emosi dan pemikiran layaknya seorang manusia. Tanjiro kemudian menjadi pembasmi iblis untuk mengembalikan Nezuko menjadi manusia lagi, dan untuk mencegah tragedi yang terjadi pada dia dan adiknya terulang pada orang lain',
            style: whiteTextStyle2.copyWith(fontSize: 12),
          ),
          const SizedBox(
            height: 8,
          ),
          CustomButton(
            margin: EdgeInsets.only(top: 30, bottom: 100),
            title: 'Continue To Book',
            onPressed: () {
              Get.to(SeatPage());
            },
          ),
        ],
      ),
    );
  }
}
