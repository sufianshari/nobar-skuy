part of 'pages.dart';

class OnboardingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget image() {
      return Container(
        margin: const EdgeInsets.symmetric(vertical: 63),
        width: 250,
        height: 305,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/splash.png'),
          ),
        ),
      );
    }

    Widget signWith() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 1,
            width: 90,
            decoration: BoxDecoration(color: Colors.white.withOpacity(0.5)),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 12),
            child: Text('Sign in with', style: whiteTextStyle2),
          ),
          Container(
            height: 1,
            width: 90,
            decoration: BoxDecoration(color: Colors.white.withOpacity(0.5)),
          ),
        ],
      );
    }

    Widget signUpButton() {
      return Container(
        margin: const EdgeInsets.only(top: 36, bottom: 24),
        width: double.infinity,
        height: 54,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: Colors.white.withOpacity(0.5),
            border: Border.all(color: Colors.white)),
        child: TextButton(
          onPressed: () {
            Get.to(SignUpPage());
          },
          child: Text(
            'Start with email',
            style: whiteTextStyle2.copyWith(
                fontSize: 17, color: Colors.white.withOpacity(0.9)),
          ),
        ),
      );
    }

    Widget signInButton() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Already have an account?',
              style: whiteTextStyle2.copyWith(
                  fontSize: 14, color: Colors.white.withOpacity(0.9))),
          TextButton(
            onPressed: () {
              Get.to(SignInPage());
            },
            child: Text('Sign In',
                style: whiteTextStyle2.copyWith(
                    fontSize: 14, color: Colors.white.withOpacity(0.9))),
          )
        ],
      );
    }

    return GeneralPage(
      subtitle: 'Now It’s Easy To\nBuy Movie Ticket',
      textStyle: whiteTextStyle1.copyWith(fontSize: 30),
      child: Column(
        children: [
          image(),
          signWith(),
          signUpButton(),
          signInButton(),
        ],
      ),
    );
  }
}
