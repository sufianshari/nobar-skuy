import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nobarskuy/shared/shared.dart';
import 'package:nobarskuy/ui/widgets/widgets.dart';
import 'package:get/get.dart';

part 'general_page/general_page.dart';
part 'splash_screen_page.dart';
part 'onboarding_page.dart';
part 'sign_up_page.dart';
part 'sign_in_page.dart';
part 'user_profiling_page.dart';
part 'confirm_page.dart';
part 'general_page/ilustration_page.dart';
part 'success_checkout_page.dart';
part 'success_top_up_page.dart';
part 'home_page.dart';
part 'general_page/main_page.dart';
part 'details_page.dart';
part 'seat_page.dart';
part 'checkout_page.dart';
part 'my_wallet_page.dart';
part 'profile_page.dart';
