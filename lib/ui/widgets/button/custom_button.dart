part of '../widgets.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final double width;
  final Color color;
  final BoxShadow boxShadow;
  final EdgeInsets margin;
  final Function() onPressed;

  CustomButton(
      {this.title,
      this.width = double.infinity,
      this.color = const Color(0xFFFF5E2C),
      this.boxShadow,
      this.margin = EdgeInsets.zero,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      width: width,
      height: 55,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(18),
        boxShadow: [
          BoxShadow(
            color: Colors.white.withOpacity(0.7),
            spreadRadius: 1,
            blurRadius: 100,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: TextButton(
        onPressed: onPressed,
        child: Text(title, style: whiteTextStyle2),
      ),
    );
  }
}
