part of '../widgets.dart';

class ButtonGenre extends StatelessWidget {
  final String title;
  final int status;

  ButtonGenre({this.title, this.status});

  @override
  Widget build(BuildContext context) {
    bgColor() {
      switch (status) {
        case 0:
          return Colors.transparent;
          break;
        default:
          return mainColor;
      }
    }

    return Container(
      width: 144,
      height: 60,
      decoration: BoxDecoration(
        color: bgColor(),
        borderRadius: BorderRadius.circular(18),
        border: Border.all(
          color: (status == 0) ? Colors.white : Colors.transparent,
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.white.withOpacity(0.7),
            spreadRadius: 1,
            blurRadius: 70,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Center(
        child: Text(title, style: whiteTextStyle2),
      ),
    );
  }
}
