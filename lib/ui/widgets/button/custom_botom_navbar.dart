part of '../widgets.dart';

class CustomBottomNavbar extends StatelessWidget {
  final int selectedIndex;
  final Function(int index) onTap;
  CustomBottomNavbar({this.selectedIndex = 0, this.onTap});

  @override
  Widget build(BuildContext context) {
    Widget homeButton() {
      return GestureDetector(
        onTap: () {
          if (onTap != null) {
            onTap(0);
          }
        },
        child: Container(
          width: 32,
          height: 32,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  'assets/home_ic' +
                      ((selectedIndex == 0) ? '.png' : '_normal.png'),
                ),
                fit: BoxFit.contain),
          ),
        ),
      );
    }

    Widget orderButton() {
      return GestureDetector(
        onTap: () {
          if (onTap != null) {
            onTap(1);
          }
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 83),
          width: 32,
          height: 32,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  'assets/list_ic' +
                      ((selectedIndex == 1) ? '.png' : '_normal.png'),
                ),
                fit: BoxFit.contain),
          ),
        ),
      );
    }

    Widget profileButton() {
      return GestureDetector(
        onTap: () {
          if (onTap != null) {
            onTap(2);
          }
        },
        child: Container(
          width: 32,
          height: 32,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  'assets/profile_ic' +
                      ((selectedIndex == 2) ? '.png' : '_normal.png'),
                ),
                fit: BoxFit.contain),
          ),
        ),
      );
    }

    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          blurRadius: 24,
          spreadRadius: 16,
          color: Colors.black.withOpacity(0.2),
        )
      ]),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 40.0,
            sigmaY: 40.0,
          ),
          child: Container(
            height: 60,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.2),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16),
                ),
                border: Border.all(
                  width: 1.5,
                  color: Colors.white.withOpacity(0.2),
                )),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                homeButton(),
                orderButton(),
                profileButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
