part of '../widgets.dart';

class FilmCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(right: 16),
          width: 210,
          height: 140,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(
              image: NetworkImage(
                'https://cdn0-production-images-kly.akamaized.net/5qJqGCk1744Y3FShvYIdC7n5keI=/640x853/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3485854/original/033690000_1623975380-Approved_-_A4Poster_ENG.JPG',
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 16),
          width: 210,
          height: 140,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Colors.transparent,
                Colors.black.withOpacity(0.7),
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 2, top: 86, left: 16),
                child: Text(
                  'Kimetsu No Yaiba',
                  style: whiteTextStyle2.copyWith(fontSize: 14),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16),
                child: RatingStars(4),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
