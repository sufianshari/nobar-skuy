part of '../widgets.dart';

class ArtisCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(right: 16),
          width: 70,
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(
              image: NetworkImage(
                'https://cdn.idntimes.com/content-images/duniaku/post/20191021/kimetsu-no-yaiba-zenitsu-agatsuma-flash-d93aa7d1940694f56014c35cf5d48606_600x400.jpg',
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Center(
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0, right: 16),
            child: Text(
              'Zenitsu Agatsuma',
              style: whiteTextStyle2.copyWith(fontSize: 10),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }
}
